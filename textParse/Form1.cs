﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textParse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            //Check if all fields filled in
            if (pythonText.Text != "" && inputText.Text != "" && outputText.Text != "" && delimeterText.Text != "" && delimCountText.Text != "" && scriptText.Text != "")
            {
                string python = @pythonText.Text;
                string myPythonApp = @scriptText.Text;
                // arg[0] = Path to your python script
                // arg[1] = input file path
                // arg[2] = output file path
                // arg[3] = delimeter
                // arg[4] = delimeter count
                ProcessStartInfo start = new ProcessStartInfo(python);
                start.UseShellExecute = false;
                start.RedirectStandardOutput = true;
                start.Arguments = myPythonApp + " " + inputText.Text + " " + outputText.Text + " " + delimeterText.Text + " " + delimCountText.Text;

                //Start process
                Process myProcess = new Process();
                myProcess.StartInfo = start;
                myProcess.Start();
                //Waits for exit signal from the app
                myProcess.WaitForExit();
                myProcess.Close();
                MessageBox.Show("Task completed.");
            }
            else
            {
                MessageBox.Show("Error, all fields must be filled in.");
            }
        }
    }
}
