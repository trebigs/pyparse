﻿namespace textParse
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.scriptText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pythonText = new System.Windows.Forms.TextBox();
            this.pythonLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.delimCountText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.delimeterText = new System.Windows.Forms.TextBox();
            this.delimeterLabel = new System.Windows.Forms.Label();
            this.outputText = new System.Windows.Forms.TextBox();
            this.outputLabel = new System.Windows.Forms.Label();
            this.inputText = new System.Windows.Forms.TextBox();
            this.inputLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(99, 183);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(101, 69);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.scriptText);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.pythonText);
            this.groupBox1.Controls.Add(this.pythonLabel);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.delimCountText);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.delimeterText);
            this.groupBox1.Controls.Add(this.delimeterLabel);
            this.groupBox1.Controls.Add(this.outputText);
            this.groupBox1.Controls.Add(this.outputLabel);
            this.groupBox1.Controls.Add(this.inputText);
            this.groupBox1.Controls.Add(this.inputLabel);
            this.groupBox1.Controls.Add(this.runButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(388, 266);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Information";
            // 
            // scriptText
            // 
            this.scriptText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.scriptText.Location = new System.Drawing.Point(100, 104);
            this.scriptText.Name = "scriptText";
            this.scriptText.Size = new System.Drawing.Size(274, 20);
            this.scriptText.TabIndex = 13;
            this.scriptText.Text = "C:\\Sandbox\\textparse\\textParse\\Properties\\textParse_3.py";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Location = new System.Drawing.Point(15, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Py Script path:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pythonText
            // 
            this.pythonText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.pythonText.Location = new System.Drawing.Point(100, 77);
            this.pythonText.Name = "pythonText";
            this.pythonText.Size = new System.Drawing.Size(274, 20);
            this.pythonText.TabIndex = 11;
            this.pythonText.Text = "C:\\Users\\Conan\\AppData\\Local\\Programs\\Python\\Python37-32\\python.exe";
            // 
            // pythonLabel
            // 
            this.pythonLabel.AutoSize = true;
            this.pythonLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.pythonLabel.Location = new System.Drawing.Point(21, 80);
            this.pythonLabel.Name = "pythonLabel";
            this.pythonLabel.Size = new System.Drawing.Size(70, 13);
            this.pythonLabel.TabIndex = 10;
            this.pythonLabel.Text = "Py EXE path:";
            this.pythonLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::textParse.Properties.Resources.conan;
            this.pictureBox1.Location = new System.Drawing.Point(216, 131);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(158, 121);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // delimCountText
            // 
            this.delimCountText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.delimCountText.Location = new System.Drawing.Point(99, 157);
            this.delimCountText.Name = "delimCountText";
            this.delimCountText.Size = new System.Drawing.Size(101, 20);
            this.delimCountText.TabIndex = 8;
            this.delimCountText.Text = "20";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Delimeter Count:";
            // 
            // delimeterText
            // 
            this.delimeterText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.delimeterText.Location = new System.Drawing.Point(100, 129);
            this.delimeterText.Name = "delimeterText";
            this.delimeterText.Size = new System.Drawing.Size(101, 20);
            this.delimeterText.TabIndex = 6;
            this.delimeterText.Text = "|";
            // 
            // delimeterLabel
            // 
            this.delimeterLabel.AutoSize = true;
            this.delimeterLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.delimeterLabel.Location = new System.Drawing.Point(37, 132);
            this.delimeterLabel.Name = "delimeterLabel";
            this.delimeterLabel.Size = new System.Drawing.Size(54, 13);
            this.delimeterLabel.TabIndex = 5;
            this.delimeterLabel.Text = "Delimeter:";
            this.delimeterLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // outputText
            // 
            this.outputText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.outputText.Location = new System.Drawing.Point(100, 50);
            this.outputText.Name = "outputText";
            this.outputText.Size = new System.Drawing.Size(274, 20);
            this.outputText.TabIndex = 4;
            this.outputText.Text = "C:\\Users\\Conan\\Desktop\\parsed.txt";
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.outputLabel.Location = new System.Drawing.Point(33, 53);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(58, 13);
            this.outputLabel.TabIndex = 3;
            this.outputLabel.Text = "Output file:";
            this.outputLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // inputText
            // 
            this.inputText.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.inputText.Location = new System.Drawing.Point(100, 24);
            this.inputText.Name = "inputText";
            this.inputText.Size = new System.Drawing.Size(274, 20);
            this.inputText.TabIndex = 2;
            this.inputText.Text = "C:\\Users\\Conan\\Desktop\\galanis.txt";
            // 
            // inputLabel
            // 
            this.inputLabel.AutoSize = true;
            this.inputLabel.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.inputLabel.Location = new System.Drawing.Point(41, 27);
            this.inputLabel.Name = "inputLabel";
            this.inputLabel.Size = new System.Drawing.Size(50, 13);
            this.inputLabel.TabIndex = 1;
            this.inputLabel.Text = "Input file:";
            this.inputLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 283);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Text Parse";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox inputText;
        private System.Windows.Forms.Label inputLabel;
        private System.Windows.Forms.TextBox delimCountText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox delimeterText;
        private System.Windows.Forms.Label delimeterLabel;
        private System.Windows.Forms.TextBox outputText;
        private System.Windows.Forms.Label outputLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox pythonText;
        private System.Windows.Forms.Label pythonLabel;
        private System.Windows.Forms.TextBox scriptText;
        private System.Windows.Forms.Label label2;
    }
}

