import re
import sys

#arg[1] = input file path
#arg[2] = output file path
#arg[3] = delimeter
#arg[4] = delimeter count

with open(sys.argv[1], "r") as f: #r for raw text helps with \t delimeter
    with open(sys.argv[2], "w") as x:  # r for raw text helps with \t delimeter
        pipe = 1
        comment = ''
        for line in f:
            pipe = pipe + line.count(sys.argv[3])
            line = line.replace('\n', '')
            line = line.replace('\t', '')
            comment += line
            comment = re.sub(' +', ' ', comment) #removes multiple spaces within
            comment = comment.strip() # strips trailing/leading spaces
            if pipe >= int(sys.argv[4]):
                pipe = 1
                print(comment, file=x)
                comment = ''


